package net.jitix.assignment.distil.controller.worker;

import java.io.File;
import java.util.Set;
import java.util.logging.Logger;

import net.jitix.assignment.distil.common.Utils;
import net.jitix.assignment.distil.exec.Task;
import net.jitix.assignment.distil.function.DatasetAggregationFunction;
import net.jitix.assignment.distil.worker.FileAppendHandler;

public class DatasetAggregationTask implements Task {

	private static final Logger LOG = Logger.getLogger(DatasetAggregationTask.class.getCanonicalName());

	private File outputDir;
	private String recordDelimiter;
	private String key;
	private DatasetAggregationFunction[] aggregationFunctions;
	private Set<String> partitions;

	public DatasetAggregationTask(File outputDir, String recordDelimiter, String key,
			DatasetAggregationFunction[] aggregationFunctions, Set<String> partitions) {
		this.outputDir = outputDir;
		this.recordDelimiter = recordDelimiter;
		this.key = key;
		this.aggregationFunctions = aggregationFunctions;
		this.partitions = partitions;
	}

	@Override
	public String getTaskId() {
		return key;
	}

	@Override
	public Boolean call() throws Exception {
		LOG.info(String.format("Aggregating dataset for key %s", key));

		for (int i = 0; i < aggregationFunctions.length; i++) {
			DatasetAggregationFunction function = aggregationFunctions[i];
			function.init();

			// iterate over the partitions and emit the partition aggregation
			// results into the function
			for (String partition : partitions) {
				File partitionAggregationResultFile = Utils
						.getAggregatedValueFile(Utils.getPartitionDataDir(outputDir, partition), i, key);
				if (partitionAggregationResultFile.exists()) {
					function.emit(function
							.getValueFromString(Utils.readFile(partitionAggregationResultFile, recordDelimiter, true)));
				}
			}

			// write the aggregation results to file
			try (FileAppendHandler fileAppender = new FileAppendHandler(Utils.getAggregatedValueFile(outputDir, i, key),
					recordDelimiter)) {
				fileAppender.write(function.aggregate().toString());
			}
		}

		return true;
	}

}
