package net.jitix.assignment.distil.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import net.jitix.assignment.distil.collection.FileBasedSet;
import net.jitix.assignment.distil.common.Constants;
import net.jitix.assignment.distil.common.ProcessingException;
import net.jitix.assignment.distil.common.Utils;
import net.jitix.assignment.distil.config.AppConfig;
import net.jitix.assignment.distil.controller.worker.DatasetAggregationTask;
import net.jitix.assignment.distil.controller.worker.PartitionAggregationTask;
import net.jitix.assignment.distil.exec.TaskException;
import net.jitix.assignment.distil.exec.TaskExecutor;
import net.jitix.assignment.distil.function.DatasetAggregationFunction;
import net.jitix.assignment.distil.function.PartitionAggregationFunction;

/**
 * This is a singleton helper class that handles the aggregation operations on
 * the final merged dataset. It executes the partition aggregation functions in
 * parallel (on a per-partition basis). It also applies the dataset aggregation
 * functions in parallel on a per-key basis.
 * 
 * This class doesn't use the type information in the aggregation functions
 * since it uses all the functions with the same semantics.
 * 
 * @author Jit
 *
 */
public class DataAggregator {

	private static final Logger LOG = Logger.getLogger(DataAggregator.class.getCanonicalName());

	private DataAggregator() {
	}

	public static void aggregate(File mergedOutputDir, String recordDelimiter,
			DatasetAggregationFunction... aggregationFunctions) {

		Set<String> keys = new FileBasedSet(mergedOutputDir, Constants.OUTPUT_KEY_FILE_NAME, recordDelimiter);
		Set<String> partitions = new FileBasedSet(mergedOutputDir, Constants.OUTPUT_PARTITION_FILE_NAME,
				recordDelimiter);

		applyPartitionAggregationFunctions(mergedOutputDir, recordDelimiter, aggregationFunctions, keys, partitions);

		applyDatasetAggregationFunctions(mergedOutputDir, recordDelimiter, keys, aggregationFunctions, partitions);

	}

	private static void applyPartitionAggregationFunctions(File mergedOutputDir, String recordDelimiter,
			DatasetAggregationFunction[] aggregationFunctions, Set<String> keys, Set<String> partitions) {
		List<PartitionAggregationTask> partitionAggregationTasks = new ArrayList<>();

		for (String partition : partitions) {
			// get new instances of the partition aggregation functions for each
			// partition since the functions are stateful
			List<PartitionAggregationFunction> partitionAggregationFunctions = getPartitionAggregationFunctions(
					aggregationFunctions);

			partitionAggregationTasks
					.add(new PartitionAggregationTask(Utils.getPartitionDataDir(mergedOutputDir, partition), keys,
							recordDelimiter, partitionAggregationFunctions));
		}

		try {
			new TaskExecutor(partitionAggregationTasks, AppConfig.getPartitionAggregationThreadCount(),
					AppConfig.getPartitionAggregationTimeoutSec()).execute();
		} catch (TaskException e) {
			throw new ProcessingException("Error executing partition aggregation tasks", e);
		}
	}

	private static List<PartitionAggregationFunction> getPartitionAggregationFunctions(
			DatasetAggregationFunction[] aggregationFunctions) {
		List<PartitionAggregationFunction> partitionAggregationFunctions = new ArrayList<>(aggregationFunctions.length);
		for (DatasetAggregationFunction datasetAggregationFunction : aggregationFunctions) {
			partitionAggregationFunctions.add(datasetAggregationFunction.getPartitionValueAggregator());
		}
		return partitionAggregationFunctions;
	}

	private static void applyDatasetAggregationFunctions(File outputDir, String recordDelimiter, Set<String> keys,
			DatasetAggregationFunction[] aggregationFunctions, Set<String> partitions) {

		List<DatasetAggregationTask> aggregationTasks = new ArrayList<>();

		for (String key : keys) {
			// create a copy of the function array so that each parallel task
			// has it's own copy, which is necessary because the functions are
			// stateful to support streaming operation
			aggregationTasks.add(new DatasetAggregationTask(outputDir, recordDelimiter, key,
					getCopy(aggregationFunctions), partitions));
		}

		try {
			new TaskExecutor(aggregationTasks, AppConfig.getDatasetAggregationThreadCount(),
					AppConfig.getDatasetAggregationTimeoutSec()).execute();
		} catch (TaskException e) {
			throw new ProcessingException("Error executing dataset aggregation tasks", e);
		}

	}

	private static DatasetAggregationFunction[] getCopy(DatasetAggregationFunction[] functions) {
		DatasetAggregationFunction[] copiedFunctions = new DatasetAggregationFunction[functions.length];
		for (int i = 0; i < functions.length; i++) {
			try {
				// the copy is done using reflection which is ok because the
				// number of functions will be much less than the number of
				// unique keys
				copiedFunctions[i] = functions[i].getClass().newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				throw new ProcessingException(String.format("Error creating instance of function class %s",
						functions[i].getClass().getCanonicalName()));
			}
		}

		return copiedFunctions;
	}

}
