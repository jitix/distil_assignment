package net.jitix.assignment.distil.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Logger;

import net.jitix.assignment.distil.common.Utils;
import net.jitix.assignment.distil.controller.worker.MapDelegationTask;
import net.jitix.assignment.distil.function.DatasetAggregationFunction;
import net.jitix.assignment.distil.worker.OutputMerger;

/**
 * Helper class for the main class. It delegates the processing of the files to
 * the executor, merges the output and runs the aggregation functions on the
 * merged output.
 * 
 * @author Jit
 *
 */
public class InputProcessor {

	private static final Logger LOG = Logger.getLogger(InputProcessor.class.getCanonicalName());

	// prevent instance creation
	private InputProcessor() {

	}

	public static void process(File workingDir, Queue<File> inputFiles, String recordDelimiter,
			DatasetAggregationFunction[] aggregationFunctions) {

		// run executor processes in parallel and check results
		List<MapDelegationTask> successfulTasks = ParallelExecutionDelegator.executeInParallel(workingDir, inputFiles);

		// Merge the key list files while removing duplicates, and merge the
		// files containing values on a per-partition basis
		File mergedOutputDir = mergeMapOutputFiles(successfulTasks, workingDir, recordDelimiter);

		// run aggregation functions
		DataAggregator.aggregate(mergedOutputDir, recordDelimiter, aggregationFunctions);
	}

	private static File mergeMapOutputFiles(List<MapDelegationTask> delegatorTasks, File workingDir,
			String recordDelimiter) {
		File mergedOutputDir = Utils.getMergedOutputLocation(workingDir);

		List<File> sourceDirectories = new ArrayList<>();
		for (MapDelegationTask delegatorTask : delegatorTasks) {
			sourceDirectories
					.add(Utils.getMergedOutputLocationForTask(workingDir, delegatorTask.getTaskId().toString()));
		}

		new OutputMerger(sourceDirectories, mergedOutputDir, recordDelimiter).merge();

		return mergedOutputDir;
	}
}
