package net.jitix.assignment.distil.controller;

import java.io.File;
import java.util.Date;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.jitix.assignment.distil.collection.FileBasedSet;
import net.jitix.assignment.distil.common.Constants;
import net.jitix.assignment.distil.common.Utils;
import net.jitix.assignment.distil.function.DatasetAggregationFunction;
import net.jitix.assignment.distil.function.impl.AverageRequestsPerHourAggregator;
import net.jitix.assignment.distil.function.impl.MaximumRequestsPerHourAggregator;
import net.jitix.assignment.distil.function.impl.TotalRequestsAggregator;

/**
 * This is the entry point to the controller application that gathers the input
 * files and delegates the execution to the executor processes. The controller
 * app also gathers and aggregates the output from the executor processes.
 * 
 * @author Jit
 *
 */
public class ControllerApp {

	private static final Logger LOG = Logger.getLogger(ControllerApp.class.getName());

	public static void main(String[] args) {
		try {
			Date start = new Date();

			checkArguments(args);
			Utils.setupWorkingDir(args[0]);

			File workingDir = new File(args[0]);

			File inputDir = checkAndGetInputDir(args[1]);

			File[] inputFiles = inputDir.listFiles();

			// put the input files into a queue from which the delegator will
			// pick them up
			Queue<File> fileQueue = new ArrayBlockingQueue<>(inputFiles.length);
			for (File inputFile : inputFiles) {
				fileQueue.offer(inputFile);
			}

			DatasetAggregationFunction[] aggregationFunctions = new DatasetAggregationFunction[] {
					new TotalRequestsAggregator(), new AverageRequestsPerHourAggregator(),
					new MaximumRequestsPerHourAggregator() };

			InputProcessor.process(workingDir, fileQueue, Constants.RECORD_DELIMITER, aggregationFunctions);

			displayResults(workingDir, aggregationFunctions);

			Date end = new Date();

			System.out.println("-------------------------------------------");
			System.out.println("Total time taken: " + (end.getTime() - start.getTime()));

		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Error processing input files", e);
			System.exit(-1);
		}
	}

	private static void checkArguments(String[] args) {
		if (args.length != 2) {
			LOG.info(
					"Usage: java -jar controller.jar <path to working directory> <path to directory containing tsv files>");
			throw new IllegalArgumentException("All inputs not specified");
		}
	}

	private static File checkAndGetInputDir(String inputDirPath) {
		File inputDir = new File(inputDirPath);
		if (inputDir.exists()) {
			if (!inputDir.isDirectory()) {
				throw new IllegalArgumentException(String.format("Input path '%s' is not a directory", inputDirPath));
			} else {
				return inputDir;
			}
		} else {
			throw new IllegalArgumentException(String.format("Input directory '%s' does not exist", inputDirPath));
		}
	}

	private static void displayResults(File workingDir, DatasetAggregationFunction[] aggregationFunctions) {
		File outputDir = Utils.getMergedOutputLocation(workingDir);
		Set<String> keySet = new FileBasedSet(outputDir, Constants.OUTPUT_KEY_FILE_NAME, Constants.RECORD_DELIMITER);

		for (String key : keySet) {
			System.out.println("-------------------------------------------");
			System.out.println(String.format("Domain '%s'", key));

			for (int i = 0; i < aggregationFunctions.length; i++) {
				System.out.println(String.format("%s: %s", aggregationFunctions[i].getFunctionName(), Utils
						.readFile(Utils.getAggregatedValueFile(outputDir, i, key), Constants.RECORD_DELIMITER, true)));
			}
		}

	}
}
