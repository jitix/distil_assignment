package net.jitix.assignment.distil.controller.worker;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.jitix.assignment.distil.common.Utils;
import net.jitix.assignment.distil.exec.Task;
import net.jitix.assignment.distil.worker.StreamReader;

/**
 * This class represents a task that spawns an executor process, executes it and
 * redirects the logs to the log output directory.
 * 
 * @author Jit
 *
 */
public class MapDelegationTask implements Task {

	private static final Logger LOG = Logger.getLogger(MapDelegationTask.class.getCanonicalName());

	private UUID taskId;
	private File workingDir;
	private File[] inputFiles;

	public MapDelegationTask(UUID taskId, File workingDir, File[] inputFiles) {
		this.taskId = taskId;
		this.workingDir = workingDir;
		this.inputFiles = inputFiles;
	}

	public File getWorkingDir() {
		return workingDir;
	}

	public File[] getInputFiles() {
		return inputFiles;
	}

	@Override
	public String getTaskId() {
		return taskId.toString();
	}

	@Override
	public Boolean call() {

		//setup the task data dir
		Utils.setupDirectory(Utils.getTaskDataLocation(workingDir, getTaskId()));
		
		// start the executor process
		Process executorProcess = null;
		try {
			// get the command array based on the object's fields
			String[] cmdArray = getCommandArray();

			LOG.info(String.format("Command to be executed: %s", Arrays.toString(cmdArray)));

			// execute process and redirect logs
			executorProcess = Runtime.getRuntime().exec(cmdArray);
			StreamReader stdOutReader = new StreamReader(Utils.getLogDir(workingDir,getTaskId()), "stdout",
					executorProcess.getInputStream());
			StreamReader stdErrReader = new StreamReader(Utils.getLogDir(workingDir,getTaskId()), "stderr",
					executorProcess.getErrorStream());
			stdOutReader.start();
			stdErrReader.start();

			// wait for execution to complete and handle result
			int returnStatus = executorProcess.waitFor();
			LOG.info(String.format("Execution of executor process completed, status=%d", returnStatus));

			if (returnStatus != 0) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Error executing map tasks", e);
			return false;
		} finally {
			if (executorProcess != null) {
				executorProcess.destroy();
			}
		}
	}

	private String[] getCommandArray() {
		List<String> cmdList = new ArrayList<>(
				Arrays.asList("java", "-jar", Utils.getJarPath().getParentFile().getAbsolutePath() + "/executor.jar",
						getTaskId(), workingDir.getAbsolutePath()));
		for (File inputFile : inputFiles) {
			cmdList.add(inputFile.getAbsolutePath());
		}
		return cmdList.toArray(new String[cmdList.size()]);
	}

	@Override
	public String toString() {
		return "MapDelegator [workingDir=" + workingDir + ", inputFiles=" + Arrays.toString(inputFiles) + "]";
	}

}
