package net.jitix.assignment.distil.controller.worker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Logger;

import net.jitix.assignment.distil.common.Utils;
import net.jitix.assignment.distil.exec.Task;
import net.jitix.assignment.distil.function.PartitionAggregationFunction;
import net.jitix.assignment.distil.worker.FileAppendHandler;

public class PartitionAggregationTask implements Task {

	private static final Logger LOG = Logger.getLogger(PartitionAggregationTask.class.getCanonicalName());

	private File partitionDataDir;

	private Set<String> keySet;
	private String recordDelimiter;

	private List<PartitionAggregationFunction> aggregationFunctions;

	public PartitionAggregationTask(File partitionDataDir, Set<String> keySet, String recordDelimiter,
			List<PartitionAggregationFunction> aggregationFunctions) {
		this.partitionDataDir = partitionDataDir;
		this.keySet = keySet;
		this.recordDelimiter = recordDelimiter;
		this.aggregationFunctions = aggregationFunctions;
	}

	@Override
	public String getTaskId() {
		return partitionDataDir.toString();
	}

	@Override
	public Boolean call() throws Exception {
		for (String key : keySet) {
			// look for the values in the partition dir
			File valuesFile = Utils.getOutputValueFile(partitionDataDir, key);
			if (valuesFile.exists()) {
				List<Object> aggregatedValues = scanAndAggregateValues(valuesFile);
				writeAggregatedValuesForKey(key, aggregatedValues);
			}
		}

		return true;
	}

	private void writeAggregatedValuesForKey(String key, List<Object> aggregatedValues) throws IOException {
		for (int i = 0; i < aggregatedValues.size(); i++) {
			File aggregatedValueFile = Utils.getAggregatedValueFile(partitionDataDir, i, key);
			try (FileAppendHandler fileAppender = new FileAppendHandler(aggregatedValueFile, recordDelimiter)) {
				fileAppender.write(aggregatedValues.get(i).toString());
			}
		}
	}

	private List<Object> scanAndAggregateValues(File valuesFile) throws FileNotFoundException {

		resetAggregationFunctions();

		List<Object> aggregatedValues = new ArrayList<>(aggregationFunctions.size());

		try (Scanner valueFileScanner = new Scanner(valuesFile)) {
			valueFileScanner.useDelimiter(recordDelimiter);
			while (valueFileScanner.hasNextLine()) {
				String valueStr = valueFileScanner.nextLine();
				for (PartitionAggregationFunction aggregationFunction : aggregationFunctions) {
					aggregationFunction.emit(valueStr);
				}
			}
		}

		for (PartitionAggregationFunction aggregationFunction : aggregationFunctions) {
			aggregatedValues.add(aggregationFunction.aggregate());
		}

		return aggregatedValues;
	}

	private void resetAggregationFunctions() {
		for (PartitionAggregationFunction aggregationFunction : aggregationFunctions) {
			aggregationFunction.init();
		}
	}

}
