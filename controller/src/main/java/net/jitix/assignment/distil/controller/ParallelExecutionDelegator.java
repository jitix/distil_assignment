package net.jitix.assignment.distil.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.UUID;

import net.jitix.assignment.distil.common.ProcessingException;
import net.jitix.assignment.distil.config.AppConfig;
import net.jitix.assignment.distil.controller.worker.MapDelegationTask;
import net.jitix.assignment.distil.exec.TaskException;
import net.jitix.assignment.distil.exec.TaskExecutor;

/**
 * This is a singleton helper class that distributes the input files among the
 * executor processes. The number of files sent to each processor depends on the
 * value of the property executor.maxfiles. Each executor can get at most that
 * many files.
 * 
 * @author Jit
 *
 */
public class ParallelExecutionDelegator {

	private ParallelExecutionDelegator() {
	}

	public static List<MapDelegationTask> executeInParallel(File workingDir, Queue<File> inputFiles) {

		List<MapDelegationTask> delegatorTasks = new ArrayList<>();

		// get input files from the queue and create delegator tasks
		while (inputFiles.peek() != null) {
			List<File> delegatorFileList = new ArrayList<>(AppConfig.getMaxFilesPerExecutor());
			int counter = AppConfig.getMaxFilesPerExecutor();
			while (counter > 0) {
				if (inputFiles.peek() != null) {
					delegatorFileList.add(inputFiles.poll());
				}
				counter--;
			}

			UUID taskId = UUID.randomUUID();
			MapDelegationTask delegatorTask = new MapDelegationTask(taskId, workingDir,
					delegatorFileList.toArray(new File[delegatorFileList.size()]));
			delegatorTasks.add(delegatorTask);
		}

		try {
			new TaskExecutor(delegatorTasks, AppConfig.getExecutorParallelProcesses(), AppConfig.getJobTimeoutSecs())
					.execute();
		} catch (TaskException e) {
			throw new ProcessingException("Error while executing executor processes", e);
		}

		return delegatorTasks;
	}

}
