package net.jitix.assignment.distil.function.impl;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AverageRequestsPerHourAggregatorTest {

	private AverageRequestsPerHourAggregator sut;

	@Before
	public void setUp() {
		sut = new AverageRequestsPerHourAggregator();
	}

	@Test
	public void testFunctionName() {
		Assert.assertEquals("Average Requests Per Hour", sut.getFunctionName());
	}

	@Test
	public void testInitialValue() {
		Assert.assertEquals(0, (double) sut.aggregate(),0.1);
	}

	@Test
	public void testAggregate() {
		sut.emit(5);
		Assert.assertEquals(5, (double) sut.aggregate(),0.1);

		sut.emit(2);
		Assert.assertEquals(3.5, (double) sut.aggregate(),0.1);

		sut.emit(-1);// value is supposed to be unchanged for negative input
		Assert.assertEquals(3.5, (double) sut.aggregate(),0.1);
		
		sut.emit(3);
		Assert.assertEquals(3.3, (double) sut.aggregate(),0.1);
		
	}
}
