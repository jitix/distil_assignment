package net.jitix.assignment.distil.function.impl;

import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RecordMapperTest {

	private RecordMapper sut;

	@Before
	public void setUp() {
		sut = new RecordMapper("\t");
	}

	@Test
	public void testMapWithInvalidTimestamp() {
		Map<String, Long> result = sut.map("147abc52	weather.com	weather.com/page66.html");

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testMapWithInvalidDelimiter() {
		Map<String, Long> result = sut.map("1475245663.211,weather.com,weather.com/page66.html");

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testMapWithInvalidDomain() {
		Map<String, Long> result = sut.map("1475245663.211		weather.com/page66.html");

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testMap() {
		// test 3 digit milliseconds part
		Map<String, Long> result = sut.map("1475245663.211	weather.com	weather.com/page66.html");

		Assert.assertEquals(1, result.size());

		Assert.assertEquals(1475245663000L, (long) result.get("weather.com"));

		// test 2 digit milliseconds part
		Map<String, Long> result2 = sut.map("1475211648.14	google.com	google.com/page49.html");
		Assert.assertEquals(1, result2.size());

		Assert.assertEquals(1475211648000L, (long) result2.get("google.com"));

		// test 1 digit milliseconds part
		Map<String, Long> result3 = sut.map("1475251888.9	linkedin.com	linkedin.com/page88.html");
		Assert.assertEquals(1, result3.size());

		Assert.assertEquals(1475251888000L, (long) result3.get("linkedin.com"));
	}

	@Test
	public void testMapWithInvalidURL() {
		Map<String, Long> result = sut.map("1475245663.211	weather.com	........");

		Assert.assertEquals(1, result.size());

		Assert.assertEquals(1475245663000L, (long) result.get("weather.com"));
	}

}
