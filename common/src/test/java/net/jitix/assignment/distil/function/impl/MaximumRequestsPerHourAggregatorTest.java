package net.jitix.assignment.distil.function.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MaximumRequestsPerHourAggregatorTest {

private MaximumRequestsPerHourAggregator sut;
	
	@Before
	public void setUp(){
		sut=new MaximumRequestsPerHourAggregator();
	}
	
	@Test
	public void testFunctionName(){
		Assert.assertEquals("Maximum Requests per Hour", sut.getFunctionName());
	}
	
	@Test
	public void testInitialValue(){
		Assert.assertEquals(0, (int)sut.aggregate());
	}
	
	@Test
	public void testAggregate(){
		sut.emit(5);
		Assert.assertEquals(5, (int)sut.aggregate());
		
		sut.emit(3);
		Assert.assertEquals(5, (int)sut.aggregate());
		
		sut.emit(9);
		Assert.assertEquals(9, (int)sut.aggregate());
	}
	
}
