package net.jitix.assignment.distil.function.impl;

import org.junit.Assert;
import org.junit.Test;

public class ValuePartitionerTest {

	@Test
	public void testGetPartition() {
		Assert.assertEquals("2016-274-7", new ValuePartitioner().getPartition(1475234355435L));

		// check with negative values
		Assert.assertEquals("1923-93-7", new ValuePartitioner().getPartition(-1475234355435L));
	}

}
