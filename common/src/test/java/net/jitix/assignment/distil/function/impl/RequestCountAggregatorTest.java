package net.jitix.assignment.distil.function.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RequestCountAggregatorTest {

	private RequestCountAggregator sut;

	@Before
	public void setUp() {
		sut = new RequestCountAggregator();
	}

	@Test
	public void testInitialValue() {
		Assert.assertEquals(0, (int) sut.aggregate());
	}
	
	@Test
	public void testAggregateWithInvalidInput(){
		sut.emit("");
		Assert.assertEquals(0, (int) sut.aggregate());
	}

	@Test
	public void testAggregate() {
		sut.emit("5");
		Assert.assertEquals(1, (int) sut.aggregate());

		sut.emit("3");
		Assert.assertEquals(2, (int) sut.aggregate());

		sut.emit("1");
		Assert.assertEquals(3, (int) sut.aggregate());
	}

}
