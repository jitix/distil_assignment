package net.jitix.assignment.distil.util;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.UUID;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import net.jitix.assignment.distil.collection.FileBasedSet;
import net.jitix.assignment.distil.common.Utils;
import net.jitix.assignment.distil.worker.FileAppendHandler;

public class FileBasedSetTest {

	private static final File WORKING_LOCATION = new File(
			new File(new File(System.getProperty("user.home")), "integration-tests"), UUID.randomUUID().toString());

	@BeforeClass
	public static void setupEnv() {
		Utils.setupWorkingDir(WORKING_LOCATION.getAbsolutePath());
	}

	@Test
	public void testEmptySet() {
		FileBasedSet set = new FileBasedSet(WORKING_LOCATION, "test1", "\n");

		Assert.assertTrue(new File(WORKING_LOCATION, "test1").exists() && new File(WORKING_LOCATION, "test1").isFile());
		Assert.assertTrue(new File(WORKING_LOCATION, "test1-entries").exists()
				&& new File(WORKING_LOCATION, "test1-entries").isDirectory());

		set.add("abc");

		Assert.assertTrue(new File(new File(WORKING_LOCATION, "test1-entries"), Utils.getHash("abc")).exists()
				&& new File(new File(WORKING_LOCATION, "test1-entries"), Utils.getHash("abc")).isFile());

		set.add("abcd");
		set.add("abcde");

		Assert.assertEquals(3, set.size());
		
		Assert.assertTrue(set.contains("abcd"));
		
		Assert.assertTrue(set.containsAll(Arrays.asList("abcd","abcde")));

	}
	
	@Test
	public void testExistingSet() throws IOException{
		try(FileAppendHandler appendHandler=new FileAppendHandler(new File(WORKING_LOCATION,"test2"), "\n")){
			appendHandler.write("123");
			appendHandler.write("1234");
		}
		
		FileBasedSet set=new FileBasedSet(WORKING_LOCATION, "test2", "\n");
		Assert.assertEquals(2, set.size());
		
		Assert.assertTrue(new File(new File(WORKING_LOCATION, "test2-entries"), Utils.getHash("123")).exists()
				&& new File(new File(WORKING_LOCATION, "test2-entries"), Utils.getHash("123")).isFile());
		
		Assert.assertTrue(new File(new File(WORKING_LOCATION, "test2-entries"), Utils.getHash("1234")).exists()
				&& new File(new File(WORKING_LOCATION, "test2-entries"), Utils.getHash("1234")).isFile());
	}

	@AfterClass
	public static void teardownEnv() {
		Utils.delete(WORKING_LOCATION);
	}

}
