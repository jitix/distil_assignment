package net.jitix.assignment.distil.function.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TotalRequestsAggregatorTest {

	private TotalRequestsAggregator sut;

	@Before
	public void setUp() {
		sut = new TotalRequestsAggregator();
	}

	@Test
	public void testFunctionName() {
		Assert.assertEquals("Total Requests", sut.getFunctionName());
	}

	@Test
	public void testInitialValue() {
		Assert.assertEquals(0, (int) sut.aggregate());
	}

	@Test
	public void testAggregate() {
		sut.emit(5);
		Assert.assertEquals(5, (int) sut.aggregate());

		sut.emit(3);
		Assert.assertEquals(8, (int) sut.aggregate());

		sut.emit(-1);// value is supposed to be unchanged for negative input
		Assert.assertEquals(8, (int) sut.aggregate());

		sut.emit(1);
		Assert.assertEquals(9, (int) sut.aggregate());
	}
}
