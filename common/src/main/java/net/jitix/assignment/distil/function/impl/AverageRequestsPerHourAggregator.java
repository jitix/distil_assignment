package net.jitix.assignment.distil.function.impl;

import net.jitix.assignment.distil.function.DatasetAggregationFunction;
import net.jitix.assignment.distil.function.PartitionAggregationFunction;

public class AverageRequestsPerHourAggregator implements DatasetAggregationFunction<Double, Integer> {

	private int sum;
	private int count;

	public AverageRequestsPerHourAggregator() {
		init();
	}

	@Override
	public void init() {
		sum = 0;
		count = 0;
	}

	@Override
	public String getFunctionName() {
		return "Average Requests Per Hour";
	}

	@Override
	public void emit(Integer value) {
		if (value >= 0) {// hourly request count cannot be less than 0
			sum += value;
			count++;
		}
	}

	@Override
	public Double aggregate() {
		if (count == 0) {// if no data has been streamed then return 0
			return 0.0;
		} else {
			return sum / (double) count;
		}

	}

	@Override
	public PartitionAggregationFunction<Integer> getPartitionValueAggregator() {
		return new RequestCountAggregator();
	}

	@Override
	public Integer getValueFromString(String valueStr) {
		return Integer.parseInt(valueStr);
	}

}
