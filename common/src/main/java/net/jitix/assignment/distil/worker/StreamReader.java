package net.jitix.assignment.distil.worker;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.jitix.assignment.distil.common.Utils;

public class StreamReader extends Thread {
	
	private static final Logger LOG = Logger.getLogger(StreamReader.class.getCanonicalName());

	private String name;
	private InputStream inStream;
	
	private File logFile;

	public StreamReader(File logDir, String name, InputStream inStream) {
		this.name = name;
		this.inStream = inStream;
		this.logFile=Utils.getLogFileLocation(logDir,name);
		LOG.info(String.format("Log file: %s",logFile));
	}

	@Override
	public void run() {
		try (InputStreamReader streamReader = new InputStreamReader(inStream);
				BufferedReader bufferedReader = new BufferedReader(streamReader);
				FileAppendHandler appendHandler = new FileAppendHandler(logFile, "\n")) {

			while (true) {
				String s = bufferedReader.readLine();
				if (s == null)
					break;
				appendHandler.write(s);
			}
		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Error reading stream", e);
		}
	}
}