package net.jitix.assignment.distil.exec;

public class TaskException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5730310511912269104L;

	public TaskException(String message) {
		super(message);
	}

	public TaskException(String message, Throwable cause) {
		super(message, cause);
	}

}
