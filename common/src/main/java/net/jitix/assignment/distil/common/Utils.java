package net.jitix.assignment.distil.common;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.logging.Logger;

public class Utils {

	private Utils() {

	}

	private static final Logger LOG = Logger.getLogger(Utils.class.getCanonicalName());

	public static File getOutputValueFile(File outputLocation, String partition, String key) {
		return new File(new File(outputLocation, partition), getHash(key) + ".values");
	}

	public static File getOutputValueFile(File outputLocation, String key) {
		return new File(outputLocation, getHash(key) + ".values");
	}

	public static File getOutputLocationForInputFile(File workingDir, File inputFile) {
		return new File(workingDir, getHash(inputFile.getAbsolutePath()));
	}

	public static File getCompletionMarkerForInputFile(File workingDir, File inputFile) {
		return new File(workingDir, getHash(inputFile.getAbsolutePath()) + ".done");
	}

	public static File getMergedOutputLocation(File workingDir) {
		return new File(workingDir, Constants.MERGED_OUTPUT_DIR_NAME);
	}

	public static File getTaskDataLocation(File workingDir, String taskId) {
		return new File(new File(workingDir, Constants.TASK_DATA_DIR_NAME), taskId);
	}

	public static File getMergedOutputLocationForTask(File workingDir, String taskId) {
		return new File(getTaskDataLocation(workingDir, taskId), Constants.MERGED_OUTPUT_DIR_NAME);
	}

	public static File getLogFileLocation(File logDir, String name) {
		return new File(logDir, name);
	}

	public static File getLogDir(File workingDir, String taskId) {
		return new File(getTaskDataLocation(workingDir, taskId), Constants.LOG_FILE_DIR);
	}

	public static File getAggregatedValueFile(File dataDir, int functionIndex, String key) {
		return new File(dataDir, getHash(key) + "." + functionIndex + ".aggregatedValues");
	}

	public static File getPartitionDataDir(File outputDir, String partition) {
		return new File(outputDir, partition);
	}

	public static String getHash(String input) {
		try {
			byte[] hash = MessageDigest.getInstance("SHA-256").digest(input.getBytes());

			// convert to hexadecimal
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < hash.length; i++) {
				sb.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
			}

			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException("SHA 256 algorithm not found", e);
		}
	}

	public static void createFile(File file) throws IOException {
		if (file.exists()) {
			delete(file);
		}

		file.getParentFile().mkdirs();

		if (!file.createNewFile()) {
			throw new ProcessingException(String.format("Error creating new file '%s'",file.getAbsolutePath()));
		}

		LOG.fine(String.format("Created file %s",file.getAbsolutePath()));
	}

	public static boolean checkIfFileExists(File file) {
		if (file.exists()) {
			if (!file.isFile()) {
				throw new IllegalStateException(String.format("Path '%s' exists but is not a file", file.getAbsolutePath()));
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	public static void setupDirectory(File dir) {
		if (dir.exists()) {
			LOG.fine(String.format("Deleting directory '%s'",dir.getAbsolutePath()));
			delete(dir);
		}

		dir.mkdirs();
	}

	public static void delete(File file) {
		if (!file.exists()) {
			return;
		}

		if (file.isDirectory()) {
			File[] files = file.listFiles();
			for (File childFile : files) {
				delete(childFile);
			}
		}

		file.delete();
	}

	public static void setupWorkingDir(String workingDirPath) {
		// setup working dir
		File workingDir = new File(workingDirPath);
		if (!workingDir.exists()) {
			workingDir.mkdirs();
		} else {
			if (!workingDir.isDirectory()) {
				throw new IllegalArgumentException("Specified working directory path is not a directory");
			}
		}
	}

	public static File getJarPath() {
		try {
			return new File(Utils.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
		} catch (URISyntaxException e) {
			throw new IllegalStateException("Invalid jar location URI returned by the system");
		}
	}

	public static String getStreamContent(InputStream stream) {
		try (Scanner scanner = new Scanner(stream)) {
			StringBuilder sb = new StringBuilder();
			while (scanner.hasNextLine()) {
				sb.append(scanner.nextLine() + "\n");
			}
			scanner.close();
			return sb.toString();
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				throw new ProcessingException("Error getting stream content", e);
			}
		}
	}

	public static String readFile(File file, String recordDelimiter, boolean ignoreNewlines) {
		try (Scanner fileScanner = new Scanner(file)) {
			fileScanner.useDelimiter(recordDelimiter);
			StringBuilder sb = new StringBuilder();
			while (fileScanner.hasNextLine()) {
				if (ignoreNewlines) {
					sb.append(fileScanner.nextLine());
				} else {
					sb.append(fileScanner.nextLine() + recordDelimiter);
				}
			}
			return sb.toString();
		} catch (FileNotFoundException e) {
			throw new ProcessingException(String.format("Error reading contents from file '%s'",file.getAbsolutePath()), e);
		}
	}

}
