package net.jitix.assignment.distil.common;

public class ProcessingException extends RuntimeException {

	/**
	 * Generated UID
	 */
	private static final long serialVersionUID = -8920285771104034736L;

	public ProcessingException(String message) {
		super(message);
	}

	public ProcessingException(String message, Throwable cause) {
		super(message, cause);
	}

}
