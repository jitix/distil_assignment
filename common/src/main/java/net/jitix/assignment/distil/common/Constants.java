package net.jitix.assignment.distil.common;

public class Constants {

	// file and folder names
	public static final String MAP_OUTPUT_DIR_NAME = "output";
	public static final String OUTPUT_KEY_FILE_NAME = "keys";
	public static final String OUTPUT_PARTITION_FILE_NAME = "partitions";
	public static final String TASK_DATA_DIR_NAME="tasks";
	public static final String MERGED_OUTPUT_DIR_NAME="output";
	public static final String LOG_FILE_DIR = "logs";
	public static final String PROPERTIES_FILE_NAME="app.properties";
	
	// delimiters
	public static final String RECORD_DELIMITER = "\n";
	public static final String VALUE_DELIMITER = "\t";

	// property file keys
	public static final String EXECUTOR_MAX_FILES_PROP_KEY = "executor.maxfiles";
	public static final String EXECUTOR_PARALLEL_PROCESSES_PROP_KEY="executor.parallel.processes";
	public static final String EXECUTOR_PROCESSING_TIMEOUT_PROP_KEY="executor.processing.timeout";
	
	public static final String JOB_TIMEOUT_SEC_PROP_KEY="job.timeout";
	
	public static final String MERGE_THREAD_COUNT_PROP_KEY="merge.threads";
	public static final String MERGE_TIMEOUT_PROP_KEY="merge.timeout";
	
	public static final String PARTITION_AGGR_THREAD_COUNT_PROP_KEY="aggr.partition.threads";
	public static final String PARTITION_AGGR_TIMEOUT_PROP_KEY="aggr.partition.timeout";
	
	public static final String DATASET_AGGR_THREAD_COUNT_PROP_KEY="aggr.dataset.threads";
	public static final String DATASET_AGGR_TIMEOUT_PROP_KEY="aggr.dataset.timeout";


}
