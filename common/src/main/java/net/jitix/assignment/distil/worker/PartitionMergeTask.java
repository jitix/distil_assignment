package net.jitix.assignment.distil.worker;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.logging.Logger;

import net.jitix.assignment.distil.common.Utils;
import net.jitix.assignment.distil.exec.Task;

/**
 * Task that appends the contents of the source file into the destination file.
 * 
 * @author Jit
 *
 */
public class PartitionMergeTask implements Task {

	private static final Logger LOG = Logger.getLogger(PartitionMergeTask.class.getCanonicalName());

	private File sourceFile;
	private File outputFile;

	public PartitionMergeTask(File sourceFile, File outputFile) {
		this.sourceFile = sourceFile;
		this.outputFile = outputFile;
	}

	@Override
	public String getTaskId() {
		return sourceFile.getAbsolutePath();
	}

	@Override
	public Boolean call() throws Exception {
		if (!outputFile.exists()) {
			Utils.createFile(outputFile);
		}

		try (FileInputStream inStream = new FileInputStream(sourceFile);
				FileChannel inChannel = inStream.getChannel();

				FileOutputStream outStream = new FileOutputStream(outputFile, true);
				FileChannel outChannel = outStream.getChannel()) {

			long bytesCopied = outChannel.transferFrom(inChannel, outputFile.length(), inChannel.size());
			LOG.fine(String.format("Copied %d bytes from %s to %s", bytesCopied, sourceFile, outputFile));
		}
		return true;
	}

}
