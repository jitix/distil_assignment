package net.jitix.assignment.distil.worker;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.jitix.assignment.distil.common.Utils;

/**
 * This class encapsulates the different objects that are used by the map
 * function executor to append intermediate data to the files. This class also
 * encapsulates the operation of writing into the file and closing the required
 * resources.
 * 
 * This class needs to be initialized via the init() method, after which
 * subsequent calls to write() will append the passed data with the specified
 * record delimiter to the file. The close() method should be called when the
 * writing is complete and the underlying file needs to be closed.
 * 
 * @author Jit
 *
 */
public class FileAppendHandler implements AutoCloseable {

	private static final Logger LOG = Logger.getLogger(FileAppendHandler.class.getCanonicalName());

	private enum State {
		INIT, READY, CLOSED
	}

	private File outputFile;
	private FileWriter fileWriter;
	private PrintWriter printWriter;
	private String recordDelimiter;

	private State state = State.INIT;

	public FileAppendHandler(File outputFile, String recordDelimiter) throws IOException {
		this.outputFile = outputFile;
		this.recordDelimiter = recordDelimiter;
		init();
	}

	private void init() throws IOException {
		checkRequiredState(State.INIT);

		if(!outputFile.exists()){
			Utils.createFile(outputFile);
		}
		
		fileWriter = new FileWriter(outputFile, true);
		printWriter = new PrintWriter(fileWriter, true);
		state = State.READY;
	}

	public void write(String data) {
		checkRequiredState(State.READY);
		printWriter.print(data + recordDelimiter);
		printWriter.flush();
	}

	private void checkRequiredState(State requiredStatus) {
		if (state != requiredStatus) {
			throw new IllegalStateException("File append handler is not in required state " + requiredStatus);
		}
	}

	@Override
	public void close() throws IOException {
		printWriter.close();
		fileWriter.close();

		state = State.CLOSED;
	}

	@Override
	public void finalize() {
		try {
			close();
		} catch (IOException e) {
			LOG.log(Level.SEVERE, "Error closing the resources during finalize", e);
		}
	}

}
