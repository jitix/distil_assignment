package net.jitix.assignment.distil.function.impl;

import net.jitix.assignment.distil.function.DatasetAggregationFunction;
import net.jitix.assignment.distil.function.PartitionAggregationFunction;

public class MaximumRequestsPerHourAggregator implements DatasetAggregationFunction<Integer, Integer> {

	private int maxRequestsInAnHour;

	public MaximumRequestsPerHourAggregator() {
		init();
	}

	@Override
	public void init() {
		maxRequestsInAnHour = 0;
	}

	@Override
	public String getFunctionName() {
		return "Maximum Requests per Hour";
	}

	@Override
	public Integer aggregate() {
		return maxRequestsInAnHour;
	}

	@Override
	public void emit(Integer value) {
		if (value > maxRequestsInAnHour) {
			maxRequestsInAnHour = value;
		}

	}

	@Override
	public PartitionAggregationFunction<Integer> getPartitionValueAggregator() {
		return new RequestCountAggregator();
	}

	@Override
	public Integer getValueFromString(String valueStr) {
		return Integer.parseInt(valueStr);
	}

}
