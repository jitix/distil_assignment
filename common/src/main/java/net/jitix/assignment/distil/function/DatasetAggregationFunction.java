package net.jitix.assignment.distil.function;

public interface DatasetAggregationFunction<V, PV> extends AggregationFunction<V> {

	public String getFunctionName();

	public void emit(PV value);

	public PV getValueFromString(String valueStr);

	public PartitionAggregationFunction<PV> getPartitionValueAggregator();

}
