package net.jitix.assignment.distil.exec;

import java.util.concurrent.Callable;

/**
 * This is a wrapper over Callable used by the different components to define
 * tasks that are executed by the task executor. The tasks must follow the
 * semantic of either returning false or throwing exception on failure, and must
 * define a task id.
 * 
 * @author Jit
 *
 */
public interface Task extends Callable<Boolean> {

	public String getTaskId();

	@Override
	public Boolean call() throws Exception;

}
