package net.jitix.assignment.distil.collection;

public class CollectionException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -150597509979845374L;

	public CollectionException(String message) {
		super(message);
	}

	public CollectionException(String message, Throwable cause) {
		super(message, cause);
	}

}
