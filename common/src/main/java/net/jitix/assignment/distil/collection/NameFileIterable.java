package net.jitix.assignment.distil.collection;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Scanner;

import net.jitix.assignment.distil.common.ProcessingException;

public class NameFileIterable implements Iterable<String>, AutoCloseable {

	private final Scanner keyFileScanner;

	public NameFileIterable(File workingDir, String name, String recordDelimiter) {

		File nameFile = new File(workingDir, name);

		try {
			keyFileScanner = new Scanner(nameFile);
			keyFileScanner.useDelimiter(recordDelimiter);

		} catch (FileNotFoundException e) {
			throw new ProcessingException(String.format("Error opening name file '%s'", nameFile), e);
		}
	}

	@Override
	public Iterator<String> iterator() {
		return keyFileScanner;
	}

	@Override
	public void close() {
		keyFileScanner.close();
	}

}
