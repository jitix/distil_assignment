package net.jitix.assignment.distil.function.impl;

import net.jitix.assignment.distil.function.DatasetAggregationFunction;
import net.jitix.assignment.distil.function.PartitionAggregationFunction;

public class TotalRequestsAggregator implements DatasetAggregationFunction<Integer, Integer> {

	private int sum;

	public TotalRequestsAggregator() {
		init();
	}

	@Override
	public void init() {
		sum = 0;
	}

	@Override
	public String getFunctionName() {
		return "Total Requests";
	}

	@Override
	public void emit(Integer value) {
		if (value >= 0) {// hourly request count cannot be less than 0
			sum += value;
		}
	}

	@Override
	public Integer aggregate() {
		return sum;
	}

	@Override
	public PartitionAggregationFunction<Integer> getPartitionValueAggregator() {
		return new RequestCountAggregator();
	}

	@Override
	public Integer getValueFromString(String valueStr) {
		return Integer.parseInt(valueStr);
	}

}
