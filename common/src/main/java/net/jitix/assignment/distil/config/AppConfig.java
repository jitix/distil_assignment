package net.jitix.assignment.distil.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.jitix.assignment.distil.common.Constants;
import net.jitix.assignment.distil.common.Utils;

/**
 * This class contains the application config properties. During startup it
 * looks for app.properties file in the same location as the jars. If the file
 * is found then the properties found in the file are loaded and made available
 * for use.
 * 
 * If any property is not found in the app.properties file (or the file itself
 * isn't found) then it loads the missing property from the embedded
 * app.properties file.
 * 
 * 
 * @author Jit
 *
 */
public class AppConfig {

	private static final Logger LOG = Logger.getLogger(AppConfig.class.getCanonicalName());

	private static Properties externalProperties = new Properties();
	private static Properties embeddedProperties = new Properties();

	private AppConfig() {
	}

	static {
		File externalPropertiesFile = new File(Utils.getJarPath().getParentFile(), Constants.PROPERTIES_FILE_NAME);

		try {
			LOG.info(String.format("Trying to load external properties file '%s'", externalPropertiesFile));
			externalProperties.load(new FileInputStream(externalPropertiesFile));
		} catch (FileNotFoundException e) {
			LOG.info(String.format("External properties file '%s' not found. Loading embedded properties file.",
					externalPropertiesFile));
		} catch (IOException e) {
			LOG.log(Level.WARNING, String.format("Error loading app properties from file '%s'", externalPropertiesFile),
					e);
		}

		try {
			LOG.info(String.format("Trying to load embedded properties file '%s'", externalPropertiesFile));
			embeddedProperties
					.load(AppConfig.class.getClassLoader().getResourceAsStream(Constants.PROPERTIES_FILE_NAME));
		} catch (Exception e) {
			// the embedded properties file should always be present
			throw new IllegalStateException("Cannot load embedded properties file", e);
		}

		if(LOG.isLoggable(Level.FINE)){
			LOG.fine("External properties");
			printProperties(externalProperties);

			LOG.fine("Embedded properties");
			printProperties(embeddedProperties);
		}

	}

	private static void printProperties(Properties properties) {
		for (Entry<Object, Object> propertyEntry : properties.entrySet()) {
			LOG.fine(String.format("%s -> %s", propertyEntry.getKey(), propertyEntry.getValue()));
		}
	}

	public static String getPropertyValue(String propertyKey) {
		// first try to fetch the value from external file, else fall back to
		// embedded file
		return externalProperties.getProperty(propertyKey, embeddedProperties.getProperty(propertyKey));
	}

	public static int getMaxFilesPerExecutor() {
		return Integer.parseInt(getPropertyValue(Constants.EXECUTOR_MAX_FILES_PROP_KEY));
	}

	public static int getExecutorParallelProcesses() {
		return Integer.parseInt(getPropertyValue(Constants.EXECUTOR_PARALLEL_PROCESSES_PROP_KEY));
	}

	public static long getExecutorProcessingTimeoutSec() {
		return Long.parseLong(getPropertyValue(Constants.EXECUTOR_PROCESSING_TIMEOUT_PROP_KEY));
	}

	public static long getJobTimeoutSecs() {
		return Long.parseLong(getPropertyValue(Constants.JOB_TIMEOUT_SEC_PROP_KEY));
	}

	public static int getMergeThreadCount() {
		return Integer.parseInt(getPropertyValue(Constants.MERGE_THREAD_COUNT_PROP_KEY));
	}

	public static long getMergeTimeoutSec() {
		return Integer.parseInt(getPropertyValue(Constants.MERGE_TIMEOUT_PROP_KEY));
	}

	public static int getPartitionAggregationThreadCount() {
		return Integer.parseInt(getPropertyValue(Constants.PARTITION_AGGR_THREAD_COUNT_PROP_KEY));
	}

	public static long getPartitionAggregationTimeoutSec() {
		return Long.parseLong(getPropertyValue(Constants.PARTITION_AGGR_TIMEOUT_PROP_KEY));
	}

	public static int getDatasetAggregationThreadCount() {
		return Integer.parseInt(getPropertyValue(Constants.DATASET_AGGR_THREAD_COUNT_PROP_KEY));
	}

	public static long getDatasetAggregationTimeoutSec() {
		return Long.parseLong(getPropertyValue(Constants.DATASET_AGGR_TIMEOUT_PROP_KEY));
	}

}
