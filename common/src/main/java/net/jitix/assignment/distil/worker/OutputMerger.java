package net.jitix.assignment.distil.worker;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import net.jitix.assignment.distil.collection.FileBasedSet;
import net.jitix.assignment.distil.common.Constants;
import net.jitix.assignment.distil.common.ProcessingException;
import net.jitix.assignment.distil.common.Utils;
import net.jitix.assignment.distil.config.AppConfig;
import net.jitix.assignment.distil.exec.TaskException;
import net.jitix.assignment.distil.exec.TaskExecutor;

/**
 * Merges 2 or more map output directories into one. It assumes that the input
 * directories follow the standard layout of the map output directories where
 * there's one "keys" file and multiple "<key SHA checksum>.values" files. After
 * the merge is complete it creates similar files in the given output directory
 * which contain the content from the files of the same name from the input
 * directories.
 * 
 * This class does not use generics since at the file level all data is handled
 * as strings.
 * 
 * Each source directory is processed sequentially in order to avoid write
 * contention for the files written in the output directory. Within each source
 * directory the data copy tasks are run in parallel, with each task handling a
 * combination of key and partition name.
 * 
 * @author Jit
 *
 */
public class OutputMerger {

	private static final Logger LOG = Logger.getLogger(OutputMerger.class.getCanonicalName());

	private List<File> sourceDirectories;
	private File outputDirectory;
	private String recordDelimiter;

	public OutputMerger(List<File> sourceDirectories, File outputDirectory, String recordDelimiter) {
		this.sourceDirectories = sourceDirectories;
		this.outputDirectory = outputDirectory;
		this.recordDelimiter = recordDelimiter;
	}

	public void merge() {
		Utils.setupDirectory(outputDirectory);

		Set<String> destinationKeySet = new FileBasedSet(outputDirectory, Constants.OUTPUT_KEY_FILE_NAME,
				recordDelimiter);
		Set<String> destinationPartitionSet = new FileBasedSet(outputDirectory, Constants.OUTPUT_PARTITION_FILE_NAME,
				recordDelimiter);

		for (File sourceDirectory : sourceDirectories) {
			LOG.info(String.format("Merging source dir %s into %s", sourceDirectory, outputDirectory));

			if (!sourceDirectory.exists() || !sourceDirectory.isDirectory()) {
				continue;
			}

			// merge the source key set to the destination key set
			FileBasedSet srcKeySet = new FileBasedSet(sourceDirectory, Constants.OUTPUT_KEY_FILE_NAME, recordDelimiter);
			destinationKeySet.addAll(srcKeySet);

			// merge the source partition set to destination key set
			FileBasedSet srcPartitionSet = new FileBasedSet(sourceDirectory, Constants.OUTPUT_PARTITION_FILE_NAME,
					recordDelimiter);
			destinationPartitionSet.addAll(srcPartitionSet);

			try {
				mergeValueFiles(sourceDirectory, srcKeySet, srcPartitionSet);
			} catch (TaskException e) {
				throw new ProcessingException(
						String.format("Error merging output from directory '%s'", sourceDirectory), e);
			}
		}
	}

	private void mergeValueFiles(File sourceDir, Set<String> keys, Set<String> partitions) throws TaskException {

		List<PartitionMergeTask> mergeTasks = new ArrayList<>();

		for (String key : keys) {
			LOG.info(String.format("Merging values for key %s from directory '%s'", key, sourceDir));
			for (String partition : partitions) {
				File valueFile = Utils.getOutputValueFile(sourceDir, partition, key);
				if (valueFile.exists()) {
					mergeTasks.add(new PartitionMergeTask(valueFile,
							Utils.getOutputValueFile(outputDirectory, partition, key)));
				}
			}
		}

		new TaskExecutor(mergeTasks, AppConfig.getMergeThreadCount(), AppConfig.getMergeTimeoutSec()).execute();
	}

}
