package net.jitix.assignment.distil.function;

import java.util.Map;

public interface MapFunction<K, V> {

	public abstract Map<K, V> map(String record);

}
