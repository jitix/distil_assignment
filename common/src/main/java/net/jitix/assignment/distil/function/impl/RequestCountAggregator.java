package net.jitix.assignment.distil.function.impl;

import net.jitix.assignment.distil.function.PartitionAggregationFunction;

public class RequestCountAggregator implements PartitionAggregationFunction<Integer> {

	private int requestCount;

	public RequestCountAggregator() {
		init();
	}

	@Override
	public void init() {
		requestCount = 0;
	}

	@Override
	public void emit(String value) {
		if (!value.trim().isEmpty()) {
			requestCount++;
		}
	}

	@Override
	public Integer aggregate() {
		return requestCount;
	}
}
