package net.jitix.assignment.distil.function;

public interface AggregationFunction<V> {

	public void init();

	public V aggregate();

}
