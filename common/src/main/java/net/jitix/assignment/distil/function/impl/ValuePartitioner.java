package net.jitix.assignment.distil.function.impl;

import org.joda.time.DateTime;

import net.jitix.assignment.distil.function.PartitionFunction;

public class ValuePartitioner implements PartitionFunction<Long> {

	@Override
	public String getPartition(Long value) {
		DateTime date = new DateTime(value);
		return new StringBuilder().append(date.getYear()).append("-").append(date.getDayOfYear()).append("-")
				.append(date.getHourOfDay()).toString();
	}

}
