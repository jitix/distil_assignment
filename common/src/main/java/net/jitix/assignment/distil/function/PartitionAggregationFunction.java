package net.jitix.assignment.distil.function;

public interface PartitionAggregationFunction<V> extends AggregationFunction<V> {

	public void emit(String dataFromFile);
}
