package net.jitix.assignment.distil.exec;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import net.jitix.assignment.distil.common.ProcessingException;

/**
 * Utility class for executing parallel tasks over a thread pool. It submits all
 * the Task implementations and waits for the execution to complete (till
 * the given timeout). Once the jobs complete it checks the return status of the
 * tasks and if all of them are not successful then it throws a checked
 * exception so that the caller can take the appropriate actions to recover from
 * the failure.
 * 
 * The timeout must be in seconds.
 * 
 * @author Jit
 *
 */
public class TaskExecutor {

	private static final Logger LOG = Logger.getLogger(TaskExecutor.class.getCanonicalName());

	private List<? extends Task> tasks;
	private int poolSize;
	private long timeout;

	public TaskExecutor(List<? extends Task> tasks, int poolSize, long timeout) {
		this.tasks = tasks;
		this.poolSize = poolSize;
		this.timeout = timeout;
	}

	public void execute() throws TaskException {
		ExecutorService executor = Executors.newFixedThreadPool(poolSize);

		// submit tasks and store futures
		Map<String, Future<Boolean>> taskFutures = new HashMap<>();

		for (Task task : tasks) {
			taskFutures.put(task.getTaskId(), executor.submit(task));
		}

		// wait for all tasks to finish
		executor.shutdown();
		try {
			executor.awaitTermination(timeout, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			throw new TaskException("Interrupted while waiting for tasks to finish", e);
		}

		// check results
		for (Entry<String, Future<Boolean>> taskFuture : taskFutures.entrySet()) {
			try {
				if (!taskFuture.getValue().get()) {
					throw new ProcessingException(String.format("Error executing task %s", taskFuture.getKey()));
				}
			} catch (InterruptedException e) {
				throw new TaskException("Interrupted while checking task results", e);
			} catch (ExecutionException e) {
				throw new ProcessingException(String.format("Error executing task %s", taskFuture.getKey()), e);
			}
		}

	}
}
