package net.jitix.assignment.distil.function.impl;

import java.util.Collections;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import net.jitix.assignment.distil.function.MapFunction;

public class RecordMapper implements MapFunction<String, Long> {

	private static final Logger LOG = Logger.getLogger(MapFunction.class.getCanonicalName());

	private Pattern delimPattern = null;

	public RecordMapper(String recordDelimiter) {
		delimPattern = Pattern.compile(recordDelimiter);
	}

	@Override
	public Map<String, Long> map(String record) {
		try {
			String[] tokens = delimPattern.split(record);
			// java uses epoch milliseconds so removing the dot will convert the
			// timestamp string into java-compatible format.
			// since millisecond level precision is not required the dot and any
			// digits following it are replaced with 000 to handle single and
			// double digit millisecond values
			Long epochTimestampValue = Long.parseLong(tokens[0].replaceAll("\\.[0-9]*", "000").trim());

			// if the URL is empty then send empty result
			if (tokens[1].trim().isEmpty()) {
				return Collections.emptyMap();
			}
			return Collections.singletonMap(tokens[1], epochTimestampValue);
		} catch (Exception e) {
			LOG.log(Level.SEVERE, String.format("Error while processing record '%s'", record), e);
			return Collections.emptyMap();
		}
	}
}
