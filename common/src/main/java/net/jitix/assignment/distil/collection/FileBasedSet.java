package net.jitix.assignment.distil.collection;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

import net.jitix.assignment.distil.common.Utils;
import net.jitix.assignment.distil.worker.FileAppendHandler;

/**
 * This class provides a filesystem based set implementation to store String
 * objects outside of heap. The elements are stored in a name file
 * ($workingDir/$name), delimited with the specified delimiter and it maintains
 * an empty file for each element in a name entry directory
 * ($workingDir/$name-entries) for quickly checking whether an element is
 * present in the set or not. This collection is not thread safe and is intended
 * only for append/add operations. Trying to remove elements from the set is not
 * supported. The set can be cleared though. The size is maintained in an
 * internal variable to avoid scanning the name file or loading file objects
 * from the name entry directory to determine the size. Because the number of
 * elements in the set is expected to be huge toArray() methods are not
 * supported;
 * 
 * 
 * It is initialized with the working directory, a name and a record delimiter.
 * 
 * During initialization the name entry directory is cleaned and reinitialized
 * and the name file is scanned to create name entries.
 * 
 * 
 * 
 * @author Jit
 *
 */
public class FileBasedSet implements Set<String> {

	private static final String NAME_ENTRY_DIR_SUFFIX = "-entries";

	private File workingDir;

	private String name;

	private String recordDelimiter;

	private File nameFile;
	private File nameEntryDir;

	private int size;

	public FileBasedSet(File workingDir, String name, String recordDelimiter) {
		this.workingDir = workingDir;
		this.name = name;
		this.recordDelimiter = recordDelimiter;

		nameFile = new File(workingDir, name);
		nameEntryDir = new File(workingDir, name + NAME_ENTRY_DIR_SUFFIX);
		size = 0;

		init();
	}

	private void init() {
		// delete existing name entry directory if it exists
		if (nameEntryDir.exists()) {
			Utils.delete(nameEntryDir);
		}

		// check if name file exists and create name entries if required
		if (nameFile.exists()) {
			try (NameFileIterable iterable = new NameFileIterable(workingDir, name, recordDelimiter)) {
				for (String element : iterable) {
					createKeyEntry(element);
					size++;
				}
			}
		} else {
			try {
				Utils.createFile(nameFile);
			} catch (IOException e) {
				throw new CollectionException(String.format("Error creating new name file '%s'", nameFile), e);
			}
			Utils.setupDirectory(nameEntryDir);
		}
	}

	private void createKeyEntry(String element) {
		File nameEntryFile = getNameEntryFile(element);
		if (!nameEntryFile.exists()) {
			try {
				Utils.createFile(nameEntryFile);
			} catch (IOException e) {
				throw new CollectionException(
						String.format("Error creating name entry file '%s' for element %s", nameEntryFile, element), e);
			}
		}
	}

	private File getNameEntryFile(String element) {
		return new File(nameEntryDir, Utils.getHash(element));
	}

	@Override
	public boolean add(String element) {
		if (!contains(element)) {
			try (FileAppendHandler appendHandler = new FileAppendHandler(nameFile, recordDelimiter)) {
				appendHandler.write(element);
				Utils.createFile(getNameEntryFile(element));
				size++;
				return true;
			} catch (IOException e) {
				throw new CollectionException(String.format("Error adding new element '%s'", element), e);
			}
		} else {
			return false;
		}
	}

	@Override
	public boolean addAll(Collection<? extends String> elements) {
		Iterator<? extends String> iterator = elements.iterator();

		boolean modified = false;

		while (iterator.hasNext()) {
			if (add(iterator.next())) {
				modified = true;
			}
		}

		return modified;
	}

	@Override
	public void clear() {
		Utils.delete(nameFile);
		Utils.delete(nameEntryDir);
		init();
	}

	@Override
	public boolean contains(Object obj) {
		return getNameEntryFile(obj.toString()).exists();
	}

	@Override
	public boolean containsAll(Collection<?> objects) {
		Iterator<?> iterator = objects.iterator();

		while (iterator.hasNext()) {
			if (!contains(iterator.next())) {
				return false;
			}
		}

		return true;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public Iterator<String> iterator() {
		try {
			Scanner nameFileScanner = new Scanner(nameFile);
			nameFileScanner.useDelimiter(recordDelimiter);
			return nameFileScanner;
		} catch (FileNotFoundException e) {
			throw new CollectionException(String.format("Error creating file scanner for name file '%s'", nameFile), e);
		}
	}

	@Override
	public boolean remove(Object arg0) {
		throw new UnsupportedOperationException("Removing elements is not supported");
	}

	@Override
	public boolean removeAll(Collection<?> arg0) {
		throw new UnsupportedOperationException("Removing elements is not supported");
	}

	@Override
	public boolean retainAll(Collection<?> arg0) {
		throw new UnsupportedOperationException("Removing elements is not supported");
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public Object[] toArray() {
		throw new UnsupportedOperationException("Cannot convert set to array");
	}

	@Override
	public <T> T[] toArray(T[] arg0) {
		throw new UnsupportedOperationException("Cannot convert set to array");
	}

}
