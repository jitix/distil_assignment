package net.jitix.assignment.distil.function;

public interface PartitionFunction<V> {

	public String getPartition(V value);

}
