package net.jitix.assignment.distil.executor;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.jitix.assignment.distil.common.Constants;
import net.jitix.assignment.distil.common.ProcessingException;
import net.jitix.assignment.distil.common.Utils;
import net.jitix.assignment.distil.executor.worker.FileMapper;
import net.jitix.assignment.distil.function.impl.RecordMapper;
import net.jitix.assignment.distil.function.impl.ValuePartitioner;
import net.jitix.assignment.distil.worker.OutputMerger;

public class ExecutorApp {

	private static final Logger LOG = Logger.getLogger(ExecutorApp.class.getCanonicalName());

	public static void main(String[] args) {
		try {
			checkArguments(args);
			String taskId = args[0];
			File workingDir = new File(args[1]);
			File[] inputFiles = checkAndGetInputFiles(Arrays.copyOfRange(args, 2, args.length));

			// to ensure that the input files are executed in sequence a single
			// thread executor is used

			final ExecutorService executor = Executors.newSingleThreadExecutor();

			// a shutdown hooks is added with the executor to ensure that
			// running tasks are completed if SIGTERM is received
			Runtime.getRuntime().addShutdownHook(new ShutdownHook(executor));

			// the file mapping tasks are submitted and executed one by one to
			// ensure that on SIGTERM the processing of the current file can be
			// completed
			for (File inputFile : inputFiles) {
				LOG.info(String.format("Submitting task for file '%s'", inputFile));
				Future<Boolean> future = executor
						.submit(new FileMapper<String, Long>(workingDir, inputFile, Constants.RECORD_DELIMITER,
								new RecordMapper(Constants.VALUE_DELIMITER), new ValuePartitioner()));

				Boolean result = future.get();
				if (!result) {
					throw new ProcessingException(String.format("Processing unsuccessful for file '%s'", inputFile));
				}
			}

			// since all files would have been processed by now the executor
			// will not have any pending tasks
			executor.shutdownNow();

			mergeOutput(taskId, workingDir, inputFiles, Constants.RECORD_DELIMITER);

		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Error processing input file " + args[0], e);
			System.exit(-1);
		}
	}

	private static void checkArguments(String[] args) {
		if (args.length < 3) {
			LOG.info(
					"Usage: java -jar executor.jar <task Id> <path to working directory> <path to tsv file 1> <path to tsv file 2>.. <path to tsv file n>");
			throw new IllegalArgumentException("All inputs not specified");
		}
	}

	private static File[] checkAndGetInputFiles(String[] inputFilePaths) {
		File[] inputFiles = new File[inputFilePaths.length];
		for (int i = 0; i < inputFilePaths.length; i++) {
			File inputFile = new File(inputFilePaths[i]);
			if (!inputFile.exists()) {
				throw new IllegalArgumentException("Specified file path '" + inputFilePaths[i] + "' doesn't exist");
			} else {
				if (!inputFile.isFile()) {
					throw new IllegalArgumentException(
							String.format("Speficied input path '%s' is not a file", inputFilePaths[i]));
				}
			}

			inputFiles[i] = inputFile;
		}
		return inputFiles;
	}

	private static void mergeOutput(String taskId, File workingDir, File[] inputFiles, String recordDelimiter) {
		LOG.info("Merging output");
		List<File> outputDirs = new ArrayList<>(inputFiles.length);

		for (File inputFile : inputFiles) {
			outputDirs.add(Utils.getOutputLocationForInputFile(workingDir, inputFile));
		}

		new OutputMerger(outputDirs, Utils.getMergedOutputLocationForTask(workingDir, taskId), recordDelimiter).merge();
	}

	private static void cleanOldFiles(File workingDir, File[] inputFiles) {
		for (File inputFile : inputFiles) {
			Utils.delete(Utils.getOutputLocationForInputFile(workingDir, inputFile));
			Utils.delete(Utils.getCompletionMarkerForInputFile(workingDir, inputFile));
		}
	}

}
