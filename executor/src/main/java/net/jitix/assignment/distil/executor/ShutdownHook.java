package net.jitix.assignment.distil.executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.jitix.assignment.distil.config.AppConfig;

public class ShutdownHook extends Thread {

	private static final Logger LOG = Logger.getLogger(ShutdownHook.class.getCanonicalName());

	private ExecutorService executorService;

	public ShutdownHook(ExecutorService executorService) {
		super();
		this.executorService = executorService;
	}

	public void run() {
		System.out.println("SIGTERM received, waiting for current tasks to complete before exiting");
		executorService.shutdown();
		try {
			executorService.awaitTermination(AppConfig.getExecutorProcessingTimeoutSec(), TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			LOG.log(Level.SEVERE, "Error shutting down executor service", e);
		}
	}

}
