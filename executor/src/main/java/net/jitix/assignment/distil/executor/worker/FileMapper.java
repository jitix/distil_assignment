package net.jitix.assignment.distil.executor.worker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Logger;

import net.jitix.assignment.distil.collection.FileBasedSet;
import net.jitix.assignment.distil.common.Constants;
import net.jitix.assignment.distil.common.ProcessingException;
import net.jitix.assignment.distil.common.Utils;
import net.jitix.assignment.distil.exec.Task;
import net.jitix.assignment.distil.function.MapFunction;
import net.jitix.assignment.distil.function.PartitionFunction;
import net.jitix.assignment.distil.worker.FileAppendHandler;

/**
 * The file mapper is a wrapper over an implementation of the MapFunction
 * interface that reads the input file line by line, executes the map function
 * code and stores the output data in the intermediate files. It creates one
 * file per key emitted from the map function and each file is a series of
 * newline delimited values which correspond to the values emitted from the map
 * function.
 * 
 * @author Jit
 *
 */
public class FileMapper<K, V> implements Task {

	private static final Logger LOG = Logger.getLogger(FileMapper.class.getCanonicalName());

	private File workingDir;
	private File inputFile;
	private String recordDelimiter;
	private MapFunction<K, V> mapFunction;
	private PartitionFunction<V> partitionFunction;

	private File processingOutputLocation;

	private Set<String> keySet;
	private Set<String> partitionSet;

	public FileMapper(File workingDir, File inputFile, String recordDelimiter, MapFunction<K, V> mapFunction,
			PartitionFunction<V> partitionFunction) {
		this.workingDir = workingDir;
		this.inputFile = inputFile;
		this.recordDelimiter = recordDelimiter;
		this.mapFunction = mapFunction;
		this.partitionFunction = partitionFunction;

		processingOutputLocation = Utils.getOutputLocationForInputFile(workingDir, inputFile);
		keySet = new FileBasedSet(processingOutputLocation, Constants.OUTPUT_KEY_FILE_NAME, recordDelimiter);
		partitionSet = new FileBasedSet(processingOutputLocation, Constants.OUTPUT_PARTITION_FILE_NAME,
				recordDelimiter);
	}

	@Override
	public String getTaskId() {
		return Utils.getHash(inputFile.getAbsolutePath());
	}

	@Override
	public Boolean call() {
		LOG.info(String.format("Processing file '%s'.", inputFile.getAbsolutePath()));

		// if the completion marker file exists then skip processing
		if (Utils.checkIfFileExists(Utils.getCompletionMarkerForInputFile(workingDir, inputFile))) {
			LOG.info("File has already been processed. Skipping.");
			return true;
		} else {
			// clean and setup the output directory
			Utils.setupDirectory(processingOutputLocation);
		}

		try (Scanner fileScanner = new Scanner(inputFile);) {

			fileScanner.useDelimiter(recordDelimiter);

			while (fileScanner.hasNext()) {
				String record = fileScanner.next();
				map(record);
				Thread.sleep(100);
			}

			createMarkerFile(inputFile);

			return true;
		} catch (FileNotFoundException e) {
			throw new ProcessingException("Input file not found", e);
		} catch (InterruptedException e) {
			throw new ProcessingException("Processing thread interrupted", e);
		}
	}

	public void map(String record) {
		Map<K, V> output = mapFunction.map(record);

		serializeOutput(output);
	}

	private void serializeOutput(Map<K, V> output) {
		for (Entry<K, V> outputEntry : output.entrySet()) {
			// try to add the key to the key set
			keySet.add(outputEntry.getKey().toString());

			// apply partition function and try to add partition to partition
			// set
			String partition = partitionFunction.getPartition(outputEntry.getValue());
			partitionSet.add(partition);

			appendValueToValueFile(partition, outputEntry.getKey(), outputEntry.getValue());

		}
	}

	private void appendValueToValueFile(String partition, K key, V value) {
		File valueFile = Utils.getOutputValueFile(processingOutputLocation, partition, key.toString());
		try (FileAppendHandler valueFileAppendHandler = new FileAppendHandler(valueFile, recordDelimiter)) {
			valueFileAppendHandler.write(value.toString());
		} catch (IOException e) {
			throw new ProcessingException(String.format("Error appending value '%s' to value file '%s'",
					value.toString(), Utils.getOutputValueFile(processingOutputLocation, partition, key.toString())),
					e);
		}
	}

	private void createMarkerFile(File inputFile) {
		File markerFile = Utils.getCompletionMarkerForInputFile(workingDir, inputFile);
		try {
			// overwrite marker file if already present
			Utils.createFile(markerFile);
		} catch (IOException e) {
			throw new ProcessingException(String.format("Error while creating marker file '%s'", markerFile), e);
		}
	}

}
